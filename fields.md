+ **name:** name of the app
+ **summary:** one or two sentences about the app - normally a quote from the project site
+ **summary source:** if 'summary' is a quote, this provides the source
+ **mobile compatibility:** a number between 5 (best) and 0 (worst) to name the mobile compatibility of the app
    + 5 = perfect - nothing left to be done 
    + 4 = almost perfect, works fine with tweaks for scaling, like `scale-to-fit´ on Phosh
    + 3 = mobile compatibility could be perfect because the app was ported to other mobile platforms (e.g. Android)
    + 2 = some parts of the app are not usable with touch input on small screens
    + 1 = many parts of the app are not usable with touch input on small screens
    + 0 = app is unusable with touch input on small screens – why is this app in the Mobile GNU/Linux Apps app list?
+ **repository:** link to the code repository
+ **website:** link to the project‘s website
+ **screenshots:** links to sites providing screenshots of the application
+ **license:** license of the project
+ **framework:** UI frameworks which are used by the app
+ **more information:** links to sites providing more information about the app
+ **description:** more detailed text about the app - normally a quote from the project site
+ **description source:** if 'description' is a quote, this provides the source
+ **backend:** backends which are used by the app
+ **service:** services the app includes and makes use of
+ **appid:** AppStream ID in the form {tld}.{vendor}.{product} (e.g. org.kde.itinerary)
+ **notice:** additional information about develpment stage, mobile compatibility or what else is important to know
+ **flatpak:** link to the Flathub page (if available) or another location the app could be installed from as flatpak

# In the website order
+ **name:** name of the app
+ **category:** App category (one or two words)
+ **summary:** one or two sentences about the app - normally a quote from the project site
+ **mobile compatibility:** a number between 0 (best) and 4 (worst) to name the mobile compatibility of the app
    + 5 = perfect - nothing left to be done 
    + 4 = almost perfect, works fine with tweaks for scaling, like `scale-to-fit` on Phosh
    + 3 = mobile compatibility could be perfect because the app was ported to other mobile platforms (e.g. Android)
    + 2 = some parts of the app are not usable with touch input on small screens
    + 1 = many parts of the app are not usable with touch input on small screens
    + 0 = app is unusable with touch input on small screens – why is this app in the Mobile GNU/Linux Apps app list?
+ **repository:** link to the code repository
+ **framework:** UI frameworks which are used by the app
+ **screenshots:** links to sites providing screenshots of the application
+ **website:** link to the project‘s website
+ **license:** license of the project
+ **appid:** AppStream ID in the form {tld}.{vendor}.{product} (e.g. org.kde.itinerary)
+ **flatpak:** link to the Flathub page (if available) or another location the app could be installed from as flatpak
+ **notice:** additional information about develpment stage, mobile compatibility or what else is important to know
+ **more information:** links to sites providing more information about the app
+ **backend:** backends which are used by the app
+ **service:** services the app includes and makes use of
+ **description:** more detailed text about the app - normally a quote from the project site
+ **description source:** if 'description' is a quote, this provides the source
+ **summary source:** if 'summary' is a quote, this provides the source
